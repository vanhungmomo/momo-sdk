import { createStackNavigator, createAppContainer } from 'react-navigation';
import HomeScreen from '../screen/HomeScreen';
const AppNavigator = createStackNavigator({
    HomeScreen: HomeScreen
});
export default createAppContainer(AppNavigator);
