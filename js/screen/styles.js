import React, { Component } from 'react';
import { Dimensions } from 'react-native';
let { width, height } = Dimensions.get('window');
export default {
    container: {
        flex: 1,
        backgroundColor: '#EDEEEE'
    },
    textInput: {
        fontSize: 30,
        marginHorizontal: 15,
        marginTop: 5,
        height: 50,
        paddingBottom: 2,
        borderBottomColor: '#dadada',
        borderBottomWidth: 1,
    },
    formInput: {
        backgroundColor: '#FFF',
        flex: 1

    },
    topContainner: {
        backgroundColor: '#b0006d',
        alignItems: 'center',
        paddingVertical: 20,
    },
    scanner: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'transparent'
    },
    topButtonImage: {
        width: 15,
        height: 15,
        justifyContent: 'center',
        resizeMode: 'contain'
    },
    topButtonText: {
        fontSize: 12,
        color: 'white',
        marginLeft: 5,
        backgroundColor: 'transparent'
    },
    topButtonContainer: {
        height: 36,
        width: 100,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'white',
        borderWidth: 1,
        borderRadius: 18
    },
    buttonUpdateQrcode: {
        marginHorizontal: 15, marginTop: 0,
        borderWidth: 1,
        borderColor: '#BCC2C6',
        backgroundColor: 'white',
        borderRadius: 4
    },
    buttonRefund: {
        marginHorizontal: 15, marginTop: 0,
        borderWidth: 1,
        borderColor: '#BCC2C6',
        backgroundColor: 'white',
        borderRadius: 4,

    }
};