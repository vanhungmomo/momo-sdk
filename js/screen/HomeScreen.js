import React, { Component } from 'react';
import { View, TouchableOpacity, Text, Platform, DeviceEventEmitter, NativeModules, NativeEventEmitter, TextInput, Dimensions, Clipboard, Keyboard, Linking } from 'react-native';
import styles from './styles';
import RNMomosdk from 'react-native-momosdk';
import HttpUtils from '../common/HttpUtils';
import AsyncStorage from '@react-native-community/async-storage';
const RNMomosdkModule = NativeModules.RNMomosdk;
const EventEmitter = new NativeEventEmitter(RNMomosdkModule);
let { width } = Dimensions.get("window");
const extraData = "{\n" +
    "    \"is_discounted\": false,\n" +
    "    \"site_code\": \"033\",\n" +
    "    \"site_name\": \"CGV Liberty Tan Binh\",\n" +
    "    \"screen_name\": \"normal\",\n" +
    "    \"session_time\": \"21:30\",\n" +
    "    \"session_date\": \"2018-04-11\",\n" +
    "    \"screen_code\": \"01\",\n" +
    "    \"movie_name\": \"IMAX3D Ready Player One\",\n" +
    "    \"movie_format\": \"2D\",\n" +
    "    \"ticket\": {\n" +
    "      \"01\": {\n" +
    "        \"type\": \"vip\",\n" +
    "        \"price\": \"75000\",\n" +
    "        \"qty\": 2\n" +
    "      }\n" +
    "    },\n" +
    "    \"concession\": {\n" +
    "      \"101568\": {\n" +
    "        \"type\": \"My Combo\",\n" +
    "        \"price\": \"70000\",\n" +
    "        \"qty\": 1\n" +
    "      }\n" +
    "    }\n" +
    "  }";
export default class HomeScreen extends Component {

    static navigationOptions = {
        header: null
    }

    state = {
        textAmount: "20.000",
        amount: 20000,
        message: "",
        dataJson: null,
        merchantname: "CGV Cinemas",
        merchantcode: "CGV19072017",
        ipAddress: "10.20.12.11"
    }

    async componentDidMount() {
        console.log("componentDidMount ok")
        const result = await AsyncStorage.getItem("IP_ADDRESS");
        if (result != null) {
            this.setState({ ipAddress: result });
        }
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenReceived', (objRes) => {
            try {
                EventEmitter.removeListener("RCTMoMoNoficationCenterRequestTokenReceived");
                console.log("<MoMoPay>Listen.Event::" + JSON.stringify(objRes));
                this.submitServer(objRes);
            } catch (ex) { }
        });
        //OPTIONAL
        EventEmitter.addListener('RCTMoMoNoficationCenterRequestTokenState', (objRes) => {
            EventEmitter.removeListener("RCTMoMoNoficationCenterRequestTokenReceived");
            // this.submitServer(objRes);
        })
    }

    componentWillUnmount() {
        EventEmitter.removeListener("RCTMoMoNoficationCenterRequestTokenReceived");
    }

    submitServer(objRes) {
        let { ipAddress, amount } = this.state;
        if (objRes && objRes.status == 0) {
            let params = {};
            params.customerNumber = objRes.phonenumber;
            params.amount = Number(amount);
            params.appData = objRes.data;
            params.billId = new Date().getTime() + "";
            params.extraData = JSON.parse(extraData);
            params.extra = "{}";
            console.log("submitServer === " + JSON.stringify(params))
            console.log(`http://${ipAddress}:8111/pay/cgv`)
            HttpUtils.fetchPost("http://" + ipAddress + ":8111/pay/cgv", params, (dataRes) => {
                console.log("submitServer == 1 ", dataRes );
                if (dataRes) {
                    this.setState({ message: JSON.stringify(dataRes) })
                } else {
                    this.setState({ message: "Có lỗi trong quá trình xử lý vui lòng thực hiện lại" })
                }
            });

        } else {
            this.setState({ message: "Đơn hàng bị hủy" })
        }
    }

    requestMoMo = () => {
        let { amount, merchantname, merchantcode } = this.state;
        Keyboard.dismiss();
        console.log("componentDidMount ok")
        // Linking.openURL("momo://?action=link&qrCodeType=104&source=websdk&type=link&amount=0&clientId=5213298&partnerCode=TIKI042017&requestType=genToken&orderId=200831MML-5213298-1598861920&dataLimitMoneyHidden=true&partnerAccountId=5213298&packageId=&userName=&customerNumber=@walletId@&partnerRefId=200831MML-5213298-1598861920&requestId=722a4649-a00a-489c-83c5-b3b0c234b8fe&deeplinkCallback=tikivn%3A%2F%2Fme-card-momo&extra=Tiki+li%C3%AAn+k%E1%BA%BFt+t%C3%A0i+kho%E1%BA%A3n+Momo&partnerClientId=5213298&formData=%7B%22body%22%3A%5B%7B%22key%22%3A%22Nh%C3%A0+cung+c%E1%BA%A5p%22%2C%22value%22%3A%22TIKI%22%2C%22isLink%22%3Atrue%7D%2C%7B%22key%22%3A%22M%C3%A3+kh%C3%A1ch+h%C3%A0ng%22%2C%22value%22%3A%225213298%22%2C%22isLink%22%3Afalse%7D%2C%7B%22key%22%3A%22M%C3%A3+giao+d%E1%BB%8Bch%22%2C%22value%22%3A%22200831MML-5213298-1598861920%22%2C%22isLink%22%3Atrue%7D%2C%7B%22key%22%3A%22N%E1%BB%99i+dung%22%2C%22value%22%3A%22Tiki+li%C3%AAn+k%E1%BA%BFt+t%C3%A0i+kho%E1%BA%A3n+Momo%22%2C%22isLink%22%3Atrue%7D%2C%7B%22key%22%3A%22T%C3%A0i+kho%E1%BA%A3n%22%2C%22value%22%3A%22%40walletId%40%22%2C%22isLink%22%3Afalse%7D%2C%7B%22key%22%3A%22T%E1%BB%95ng+ti%E1%BB%81n%22%2C%22value%22%3A%220%22%2C%22isLink%22%3Atrue%2C%22type%22%3A%22amount%22%7D%5D%7D&callbackUrl=tikivn%3A%2F%2Fme-card-momo&email=")
        this.setState({ message: "" }, async () => {
            if (amount != 0) {
                let jsonData = {};
                jsonData.environment = '0'; //"0": SANBOX , "1": PRODUCTION
                jsonData.action = 'gettoken';
                jsonData.isDev = true; //SANBOX only , remove this key on PRODUCTION
                jsonData.merchantname = merchantname;
                jsonData.merchantcode = merchantcode;
                jsonData.merchantnamelabel = "Nhà cung cấp";
                jsonData.description = "Fast and Furious 8";
                jsonData.amount = amount;
                jsonData.extraData = {};
                jsonData.orderId = 'bill234284290348';
                jsonData.orderLabel = 'Ma don hang';
                jsonData.appScheme = 'momoilpy20190524';
                console.log("data_request_payment " + JSON.stringify(jsonData));
                if (Platform.OS === 'android') {
                    let dataPayment = await RNMomosdk.requestPayment(jsonData);
                    this.momoHandleResponse(dataPayment);
                } else {
                    RNMomosdk.requestPayment(jsonData);
                }
            }
        })
    }

    async momoHandleResponse(objRes) {
        this.submitServer(objRes);
    }

    formatNumberToMoney(number, defaultNum, predicate) {
        predicate = !predicate ? "" : "" + predicate;
        if (number == 0 || number == '' || number == null || number == 'undefined' ||
            isNaN(number) === true ||
            number == '0' || number == '00' || number == '000')
            return "0" + predicate;

        var array = [];
        var result = '';
        var count = 0;

        if (!number) {
            return defaultNum ? defaultNum : "" + predicate
        }

        let flag1 = false;
        if (number < 0) {
            number = -number;
            flag1 = true;
        }

        var numberString = number.toString();
        if (numberString.length < 3) {
            return numberString + predicate;
        }

        for (let i = numberString.length - 1; i >= 0; i--) {
            count += 1;
            if (numberString[i] == "." || numberString[i] == ",") {
                array.push(',');
                count = 0;
            } else {
                array.push(numberString[i]);
            }
            if (count == 3 && i >= 1) {
                array.push('.');
                count = 0;
            }
        }

        for (let i = array.length - 1; i >= 0; i--) {
            result += array[i];
        }

        if (flag1)
            result = "-" + result;

        return result + predicate;
    }

    onChangeText = (type, value) => {
        let { textAmount, amount, ipAddress, merchantcode, merchantname } = this.state;
        let _amount = amount;
        let _textAmount = textAmount;
        let _ipAddress = ipAddress;
        let _merchantcode = merchantcode;
        let _merchantname = merchantname;
        if (type == "amount") {
            _amount = value.replace(/\./g, "").trim();
            _textAmount = this.formatNumberToMoney(_amount, null, "");
        }
        if (type == "ipAddress") {
            _ipAddress = value;
            AsyncStorage.setItem("IP_ADDRESS", _ipAddress);
        }
        if (type == "merchantcode") {
            _merchantcode = value;
        }
        if (type == "merchantname") {
            _merchantname = value;
        }
        this.setState({ amount: _amount, textAmount: _textAmount, merchantcode: _merchantcode, merchantname: _merchantname, ipAddress: _ipAddress });
    }

    writeToClipboard = async () => {
        let { message } = this.state;
        if (message) {
            await Clipboard.setString(this.state.message);
            alert('Thành công');
        }
    };

    render() {
        let { textAmount, message, ipAddress, merchantcode, merchantname } = this.state;
        return (
            <View style={{ flex: 1, marginTop: 30, alignItems: 'center' }}>
                <View style={styles.formInput}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', width: width - 30 }}>
                        <TextInput
                            autoFocus={true}
                            placeholderTextColor={"#929292"}
                            placeholder={"Nhập Ip"}
                            keyboardType={"numeric"}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            onSubmitEditing={() => { Keyboard.dismiss() }}
                            value={ipAddress}
                            style={[styles.textInput, { flex: 1, paddingRight: 30, color: '#000' }]}
                            onChangeText={(value) => this.onChangeText("ipAddress", value)}
                            underlineColorAndroid="transparent"

                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', width: width - 30 }}>
                        <TextInput
                            autoFocus={true}
                            placeholderTextColor={"#929292"}
                            placeholder={"merchantcode"}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            onSubmitEditing={() => { Keyboard.dismiss() }}
                            value={merchantcode}
                            style={[styles.textInput, { flex: 1, paddingRight: 30, color: '#000' }]}
                            onChangeText={(value) => this.onChangeText("merchantcode", value)}
                            underlineColorAndroid="transparent"
                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', width: width - 30 }}>
                        <TextInput
                            autoFocus={true}
                            placeholderTextColor={"#929292"}
                            placeholder={"merchantname"}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            onSubmitEditing={() => { Keyboard.dismiss() }}
                            value={merchantname}
                            style={[styles.textInput, { flex: 1, paddingRight: 30, color: '#000' }]}
                            onChangeText={(value) => this.onChangeText("merchantname", value)}
                            underlineColorAndroid="transparent"
                        />
                    </View>

                    <View style={{ flexDirection: 'row', alignItems: 'center', width: width - 30 }}>
                        <TextInput
                            autoFocus={true}
                            maxLength={10}
                            placeholderTextColor={"#929292"}
                            placeholder={"Nhập số tiền"}
                            keyboardType={"numeric"}
                            returnKeyType="done"
                            blurOnSubmit={true}
                            onSubmitEditing={() => { Keyboard.dismiss() }}
                            value={textAmount == 0 ? "" : textAmount}
                            style={[styles.textInput, { flex: 1, paddingRight: 30, color: '#000' }]}
                            onChangeText={(value) => this.onChangeText("amount", value)}
                            underlineColorAndroid="transparent"

                        />
                        <Text style={{ position: 'absolute', right: 20, fontSize: 30 }}>{"đ"}</Text>
                    </View>

                    <Text style={{ fontSize: 16, color: '#ce3a3e', paddingHorizontal: 15, marginTop: 5 }}>
                        {message}
                    </Text>
                    <View style={{ height: 40, marginTop: 20, marginBottom: 20, flexDirection: 'row', marginLeft: 10, marginRight: 10 }}>
                        <TouchableOpacity style={{ flex: 1, marginLeft: 5, backgroundColor: '#b0006d', justifyContent: 'center', alignItems: 'center' }} onPress={this.requestMoMo}>
                            <Text style={{ color: '#FFFFFF' }}>{"Thanh toán momo"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}