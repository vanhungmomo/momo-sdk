export default {

    fetchPost(url, params, callback) {
        let URL_Request = url;
        let headers = {
            'Content-Type': 'application/json'
        };
        let timeoutPost = 2 * 30 * 1000;
        timeout(timeoutPost, fetch(URL_Request, {
            method: 'POST',
            headers: headers,
            body: JSON.stringify(params)
        })).then((response) => {
            console.log("response", response)
            return response.json()
        })
            .then((result) => {
                console.log("data_response ", result);
                callback(result);
            })
            .catch((error) => {
                console.log(error);
                callback(false);
            }).done();

        function timeout(ms, promise) {
            return new Promise((resolve, reject) => {
                const timeoutId = setTimeout(() => {
                    reject(new Error("Request timeout"))
                }, ms);
                promise.then(
                    (res) => {
                        clearTimeout(timeoutId);
                        resolve(res);
                    },
                    (err) => {
                        clearTimeout(timeoutId);
                        reject(new Error("Connection error"));
                    }
                );
            })
        }
    },
}