import React from 'react';
import { View, Platform } from 'react-native';
import App from './App';
class setup extends React.Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <App />
            </View>

        );
    }
}
export default setup;